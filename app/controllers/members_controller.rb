class MembersController < ApplicationController
  respond_to :html, :json

  load_and_authorize_resource class: User
  
  def index
    index_json_response(MembersDatatable.new(view_context))
  end
  
  def new
  end
  
  def create
    if @member.save_and_invite_member()
      flash[:success] = "New member added and invitation email sent to #{@member.email}."
      redirect_to root_path
    else
      flash[:error] = "errors occurred!"
      render :new
    end
  end

  def edit
  end
  
  def update
    if @member.update_attributes(resource_params)
      flash[:success] = "Account updated"
      redirect_to members_path
    else
      flash[:error] = "errors occurred!"
      render :edit
    end
  end
  
  private

  def resource_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end
  
end