class TenantSerializer < ActiveModel::Serializer
  attributes :id, :name
end