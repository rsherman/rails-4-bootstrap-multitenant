class MembersDatatable < BaseDatatable
  delegate :params, :h, to: :@view

private

  def model
    User
  end
  
  def search_term
    "first_name like :search or last_name like :search or email like :search"
  end
  
  def sort_column_names
    %w[email]
  end
  
  def data
    collection.map do |item|
      [
        item.id,
        item.full_name,
        item.email
      ]
    end
  end

end