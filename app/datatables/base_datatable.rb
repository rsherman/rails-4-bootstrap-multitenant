class BaseDatatable
  delegate :params, :h, to: :@view

  def initialize(view, scope=nil)
    @scope = scope
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: model.count,
      iTotalDisplayRecords: collection.total_entries,
      aaData: data
    }
  end

private

  def model
  end

  def search_term
  end

  def sort_column_names
  end

  def data
  end

  def collection
    @collection ||= fetch_collection
  end

  def fetch_collection
    collection = model.order("#{sort_column} #{sort_direction}")
    collection = collection.page(page).per_page(per_page)
    if params[:sSearch].present?
      collection = collection.where(search_term, search: "%#{params[:sSearch]}%")
    end
    collection
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = sort_column_names
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end

end