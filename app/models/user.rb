class User < ActiveRecord::Base
  extend Enumerize
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
  
  acts_as_universal_and_determines_account
  
  # Role-based access control
  enumerize :role, in: [:client, :staff, :admin], default: :staff, scope: true, predicates: true
  
  def full_name
    [first_name, last_name].join(' ')
  end
  
end

# == Schema Information
#
# Table name: users
#
#  id                           :integer          not null, primary key
#  email                        :string(255)      default(""), not null
#  encrypted_password           :string(255)      default(""), not null
#  reset_password_token         :string(255)
#  reset_password_sent_at       :datetime
#  remember_created_at          :datetime
#  sign_in_count                :integer          default("0"), not null
#  current_sign_in_at           :datetime
#  last_sign_in_at              :datetime
#  current_sign_in_ip           :string(255)
#  last_sign_in_ip              :string(255)
#  confirmation_token           :string(255)
#  confirmed_at                 :datetime
#  confirmation_sent_at         :datetime
#  unconfirmed_email            :string(255)
#  skip_confirm_change_password :boolean          default("0")
#  tenant_id                    :integer
#  first_name                   :string(255)
#  last_name                    :string(255)
#  created_at                   :datetime
#  updated_at                   :datetime
#  role                         :string(255)
#
