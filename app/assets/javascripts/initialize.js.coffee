window.BaseApp = window.BaseApp or {}

jQuery ->
  
  $("a[rel~=popover], .has-popover").popover()
  $("a[rel~=tooltip], .has-tooltip").tooltip()

  window.template_manager = new TemplateManager()

  configuration = {}

  template_manager.load_template "hits", "/assets/templates/members/_table_row.hbs", -> 
    markup = template_manager.transform "hits", configuration
  
  Handlebars.registerHelper 'link', (url, title) ->
    return new Handlebars.SafeString("<a href='" + url + "'>" + title + "</a>")
  
  # Handlebars.registerHelper 'ifeq', (a, b, options) ->
  #   if (a === b)
  #     options.fn(this) 
  # 
  # Handlebars.registerHelper 'ifnoteq', (a, b, options) ->
  #   if (a !== b)
  #     options.fn(this)
  # 
