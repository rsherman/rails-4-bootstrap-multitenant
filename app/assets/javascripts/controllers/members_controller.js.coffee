class BaseApp.MembersController
  _dataTable: null
  constructor: () ->
    that = this
    
    @dataTable = $('#members_table').dataTable
      sPaginationType: "bootstrap"
      bJQueryUI: true
      bProcessing: true
      bServerSide: true
      sAjaxSource: $(this).data('source')
      "fnRowCallback": ( nRow, aData, iDisplayIndex ) ->
        hit = {
          id: aData[0]
          full_name: aData[1],
          email: aData[2],
        }
        markup = Handlebars.partials['members/_table_row'](hit);
        $(nRow).html(markup)
        nRow;
    
    this._dataTable = @dataTable
    
    this.setupPagination()
    
  
  setupPagination: ()->
    select = $('#members_table_length select')
    $('#members_table_length').html('')
    $('#members_table_length').text('List size')
    $('#members_table_length').append(select)
    
    $('.pagination .prev a').text('Previous')
    $('.pagination .next a').text('Next')
  

