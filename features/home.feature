Feature: Home Page
  In order to get access home page without login
  As an anonymous user i can view the home page

  Scenario: an anonymous user i can view the root url
    Given I visit the home page
    Then I should see the content for anonymous users
  
