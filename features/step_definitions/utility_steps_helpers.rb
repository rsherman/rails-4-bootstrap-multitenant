def validate_content(content, container=nil)
  if container
    within(container) { page.should have_content(content) }
  else
    page.should have_content(content)
  end
end

def validate_no_content(content, container=nil)
  if container
    within(container) { page.should_not have_content(content) }
  else
    page.should_not have_content(content)
  end
end

# Examples of waiting for a page loading to show and hide in jQuery Mobile.
def wait_for_loading
  wait_until { page.find('html')[:class].include?('ui-loading') }
rescue Capybara::TimeoutError
  flunk "Failed at waiting for loading to appear."
end
 
def wait_for_loaded
  wait_until { !page.find('html')[:class].include?('ui-loading') }
rescue Capybara::TimeoutError
  flunk "Failed at waiting for loading to complete."
end
 
def wait_for_page_load
  wait_for_loading && wait_for_loaded
end

