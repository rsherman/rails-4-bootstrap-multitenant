
Given /^I visit the home page$/ do
  visit '/'
end

Then /^I should see the content for anonymous users$/ do
  validate_content('Sign in', '.navbar')
  validate_content('Sign up', '.navbar')
  validate_no_content('My Account', '.navbar')
  validate_content('Hello, BaseAppers', '.jumbotron')
end
