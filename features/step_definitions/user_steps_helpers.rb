def create_visitor
  @visitor ||= { 
    :email => "test@yopmail.com",
    :password => "password", 
    :password_confirmation => "password", 
    :tenant_name => "test" 
  }
end

def invite_visitor
  @invite_visitor ||= { 
    :email => "test1@yopmail.com",  
    :firstname => "first name", 
    :lastname => "last name" 
  }
end

def create_password_visitor
  @password_visitor ||= {
    :password => "password", 
    :password_confirmation => "password"
  }
end

def create_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:user, 
    email: @visitor[:email], 
    password: @visitor[:password], 
    password_confirmation: @visitor[:password]
  )
end

def delete_user
  @user ||= User.where(:email => @visitor[:email]).first
  @user.destroy unless @user.nil?
end

def sign_in
  visit '/users/sign_in'
  fill_in "user_email", :with => @visitor[:email]
  fill_in "user_password", :with => @visitor[:password]
  click_button "Sign in"
end

def sign_up
  delete_user
  visit '/users/sign_up'
  fill_in "tenant_name", :with => @visitor[:tenant_name]  
  fill_in "user_email", :with => @visitor[:email]
  fill_in "user_password", :with => @visitor[:password]
  fill_in "user_password_confirmation", :with => @visitor[:password_confirmation]
  click_button "Sign up"
  find_user
end

def invite_user
  visit '/members/new'
  fill_in "user_email", :with => @invite_visitor[:email]
  fill_in "user_first_name", :with => @invite_visitor[:firstname]
  fill_in "user_last_name", :with => @invite_visitor[:lastname]
  click_button "Create user and invite"
end

def forget_password
  visit '/users/password/new'
  fill_in "user_email", :with => @visitor[:email]
  click_button "Send me reset password instructions"
end

def create_password_for_invite
  fill_in "user_password", :with => @password_visitor[:password]
  fill_in "user_password_confirmation", :with => @password_visitor[:password_confirmation]
  click_button "Create"
end

def resend_confirmation
  visit '/users/confirmation/new'
  fill_in "user_email", :with => @visitor[:email]
  click_button "Resend"
end

def find_user
  @user ||= User.where(:email => @visitor[:email]).first
end

