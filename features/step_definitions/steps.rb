
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end

Given /^the following tenant records$/ do |table|
  table.hashes.each do |hash|  
    tenant = FactoryGirl.create(:tenant)
    Thread.current[:tenant_id] = tenant
  end
end

Given /^I am logged in as "([^\"]*)" with password "([^\"]*)"$/ do |login, password|  
  visit '/users/sign_in'
  fill_in "user_email", :with => login
  fill_in "user_password", :with => password  
  click_button "Sign in"
  sleep 2
end


Then /^(?:|I )should see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end

Then /^(?:|I )should not see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_no_content(text)
  else
    assert page.has_no_content?(text)
  end
end

Then /^show me the page$/ do
  save_and_open_page
end


When /^I send valid reset password instructions$/ do
  create_visitor
  sign_up
  forget_password
end

When /^I send invalid reset password instructions$/ do
  create_visitor
  @visitor = @visitor.merge(:email => "xtest@yopmail.com")
  forget_password
end

When /^I sign up with valid user data$/ do
  create_visitor
  sign_up
end

When /^I invite user with valid data$/ do
  invite_visitor
  invite_user
end

When /^I create password with valid data$/ do
  create_password_visitor
  create_password_for_invite
end

When /^I resend confirmation with valid user data$/ do
  create_visitor
  sleep 30
  resend_confirmation
end



When /^I sign up with an invalid email$/ do
  create_visitor
  @visitor = @visitor.merge(:email => "")
  sign_up
end

When /^I sign up with an empty password$/ do
  create_visitor
  @visitor = @visitor.merge(:password => "")
  sign_up
end

When /^I sign up with a different password$/ do
  create_visitor
  @visitor = @visitor.merge(:password_confirmation => "wordpass")
  sign_up
end

When /^I sign in with a wrong email$/ do
  create_visitor
  @visitor = @visitor.merge(:email => "test1@yopmail.com")
  sign_in
end

When /^(?:|I )follow "([^"]*)"$/ do |link|
  click_link(link)
end


Then /^"([^"]*)" receives an email with "([^"]*)" as the subject$/ do |email_address, subject|
  open_email(email_address)
  current_email.subject.should eq subject
end

Then(/^show me$/) do
  save_and_open_page
end