source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'

# Use pg as the database for Active Record
gem 'mysql2', '~> 0.3.17'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.2'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

gem 'activerecord-session_store',   github: 'rails/activerecord-session_store'
gem 'active_model_serializers'
gem 'bcrypt',                       '~> 3.1.7' # Use ActiveModel has_secure_password
gem 'cancancan',                    github: 'bryanrite/cancancan', branch: 'master'
gem 'devise',                       '~> 3.4.1'
gem 'dotenv-rails',                 '~> 1.0.2'
gem 'enumerize',                    '0.6.1' 
# an issue in enumerize 0.7.0 causes app error when enumerized state is blank and scope and predicates flags are passed
gem 'exception_notification',       '~> 4.0.1'
gem 'handlebars_assets' # js template engine
gem 'jquery-datatables-rails',      github: 'rweng/jquery-datatables-rails'
# we use the custom code for datatable in app/assets/javascripts/third_party
# but we user the stylesheet included in this gem
gem 'less-rails'
gem 'milia',                        '~> 1.2.0'
gem 'newrelic_rpm'
gem 'recaptcha', require: 'recaptcha/rails'
gem 'therubyracer'
gem 'twitter-bootstrap-rails',      '~> 3.2.0'
gem 'whenever',                     '~> 0.9.0'
gem 'will_paginate'
gem 'unicorn'

# =========================================================

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

gem 'letter_opener', group: :development

group :development, :test do
  gem 'annotate'
  gem 'awesome_print'
  gem "capybara"
  gem "capybara-webkit"
  gem "codeclimate-test-reporter", require: false
  gem 'cucumber-rails', require: false
  gem 'database_cleaner'
  # gem 'debugger', group: [:development, :test]
  gem 'email_spec'
  gem 'factory_girl_rails'
  gem 'railroady'
  gem 'rspec-rails',              '~> 3.2.0'
  gem 'selenium-webdriver'
  gem 'simplecov', require: false
  gem 'spork'
  gem 'timecop'
end

# Use Capistrano for deployment
group :development do
  gem 'capistrano',             '~> 3.1.0'
  gem 'capistrano-rails',       '~> 1.1.1'
  gem 'capistrano-bundler',     '~> 1.1.2'
  gem 'capistrano-rbenv'
  gem 'capistrano-maintenance', github: "capistrano/maintenance", require: false
end
