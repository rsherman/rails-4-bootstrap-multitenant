require 'spec_helper'

describe BaseDatatable do
  
  before do
    @params = {
      :sSortDir_0 => 'asc', 
      :iSortCol_0 => '0', 
      :iDisplayStart => '0',
      :iDisplayLength => '10', 
      :sSearch => '', 
      :sEcho => '1', 
      :sColumns => '',
      :iSortingCols => '1'
    }
    @view = double('view', :params => @params)
    @dt = BaseDatatable.new(@view)
  end
  
  describe '#as_json' do
    before do
      @dt.should_receive(:model).and_return(double(:model, :count=>2))
      @dt.should_receive(:collection).and_return(double(:collection, :total_entries=>2))
    end
    it 'returns a json hash' do
      @dt.as_json.should be_a Hash
    end
    it 'has jquery.dataTables required keys' do
      @dt.as_json.keys.should include(:sEcho, :iTotalRecords, :iTotalDisplayRecords, :aaData)
    end
  end
    
  describe 'model' do
    it 'should return nil' do
      @dt.send(:model).should == nil
    end
  end
  
  describe 'search_term' do
    it 'should return nil' do
      @dt.send(:search_term).should == nil
    end
  end
  
  describe 'sort_column_names' do
    it 'should return an array of columns' do
      @dt.send(:sort_column_names).should == nil
    end
  end
  
  describe 'collection' do
    it 'should call fetch collection' do
      @dt.should_receive(:fetch_collection).and_return([1,2,3])
      @dt.send(:collection).should == [1,2,3]
    end
  end
  
  describe 'fetch_collection' do
    it 'should query model based on supplied parameters'
  end

  describe 'page' do
    it 'returns an integer that represents a page number from pagination' do
      @dt.send(:page).should be_a Integer
    end
  end

  describe 'per_page' do
    it 'returns the value of params[:iDisplayLength] converted to Integer' do
      @dt.send(:per_page).should eql 10
    end
  end

  describe 'sort_column' do
    it 'returns value of params[:iSortCol_0]' do
      @dt.should_receive(:sort_column_names).and_return(%w[name])
      
      @dt.send(:sort_column).should be_a String
    end
  end

  describe 'sort_direction' do
    it 'returns the value of params[:sSortDir_0]' do
      @dt.send(:sort_direction).should eql @view.params[:sSortDir_0]
      @dt.send(:sort_direction).should be_a String
    end
  end

end
