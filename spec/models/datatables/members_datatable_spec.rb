require 'spec_helper'

describe MembersDatatable do
  
  before do
    @dt = MembersDatatable.new(double(:view))
  end
  
  describe "model" do
    it "should return User class" do
      @dt.send(:model).should == User
    end
  end
  
  describe "search_term"do
    it "should return a string" do
      @dt.send(:search_term).should == "first_name like :search or last_name like :search or email like :search"
    end
  end
  
  describe "sort_column_names" do
    it "should return an array of columns" do
      @dt.send(:sort_column_names).should == ['email']
    end
  end
  
  describe 'data' do 
    it 'delegates call to view' do
      user = FactoryGirl.build(:user)
      @dt.should_receive(:collection).and_return([user])
      @dt.send(:data).should be_a Array
    end
  end
  

end
    
  